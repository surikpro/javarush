package com.company.wrapper_classes.ex6;

public class Solution {
    public static void main(String[] args) {
        String string = "Думаю, это будет новой фичей." +
                "Только не говорите никому, что она возникла случайно.";

        System.out.println("Количество цифр в строке : " + countDigits(string));
        System.out.println("Количество букв в строке : " + countLetters(string));
        System.out.println("Количество пробелов в строке : " + countSpaces(string));
    }

    public static int countDigits(String string) {
        int countedDigits = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                countedDigits++;
            }
        }
        return countedDigits;
    }

    public static int countLetters(String string) {
        int countedLetters = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isAlphabetic(string.charAt(i))) {
                countedLetters++;
            }
        }
        return countedLetters;
    }

    public static int countSpaces(String string) {
        int countedSpaces = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isWhitespace(string.charAt(i))) {
                countedSpaces++;
            }
        }
        return countedSpaces;
    }
}

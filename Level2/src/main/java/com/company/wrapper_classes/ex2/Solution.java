package com.company.wrapper_classes.ex2;

public class Solution {
    Byte byteValueBox;
    Short shortValueBox;
    Integer integerValueBox = 100;
    Long longValueBox;

    Float floatValueBox;
    Double doubleValueBox;

    Character characterValueBox;
    Boolean booleanValueBox;

    int byteValue = byteValueBox.byteValue();
    short shortValue = shortValueBox.shortValue();
    int intValue = integerValueBox.intValue();
    long longValue = longValueBox.longValue();
    float floatValue = floatValueBox.floatValue();
    double doubleValue = doubleValueBox.doubleValue();
    char charValue = characterValueBox.charValue();
    boolean booleanValue = booleanValueBox.booleanValue();

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.intValue);
    }
}
